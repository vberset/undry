use async_std::fs::{create_dir_all, remove_file};
use async_std::io::BufWriter;
use async_std::prelude::*;

use actix_web::error::{ErrorBadRequest, ErrorForbidden, ErrorInternalServerError, ErrorNotFound};
use actix_web::http::header::{CACHE_CONTROL, CONTENT_TYPE, IF_MODIFIED_SINCE, LAST_MODIFIED};
use actix_web::middleware::{Compress, Logger};
use actix_web::{web, App, Error as ActixError, HttpMessage, HttpResponse, HttpServer};
use blake3::Hasher;
use chrono::Utc;
use dotenv::dotenv;
use env_logger;
use envconfig::Envconfig;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

mod config;
mod error;
mod state;
mod types;

use crate::config::Config;
use crate::error::{ConfigurationError, Error};
use crate::state::AppState;
use crate::types::ObjectMetadata;

const VERSION: &str = env!("CARGO_PKG_VERSION");
static BANNER: &str = r#"
       _                  _             _            _    _        _
      /\_\               /\ \     _    /\ \         /\ \ /\ \     /\_\
     / / /         _    /  \ \   /\_\ /  \ \____   /  \ \\ \ \   / / /
     \ \ \__      /\_\ / /\ \ \_/ / // /\ \_____\ / /\ \ \\ \ \_/ / /
      \ \___\    / / // / /\ \___/ // / /\/___  // / /\ \_\\ \___/ /
       \__  /   / / // / /  \/____// / /   / / // / /_/ / / \ \ \_/
       / / /   / / // / /    / / // / /   / / // / /__\/ /   \ \ \
      / / /   / / // / /    / / // / /   / / // / /_____/     \ \ \
     / / /___/ / // / /    / / / \ \ \__/ / // / /\ \ \        \ \ \
    / / /____\/ // / /    / / /   \ \___\/ // / /  \ \ \        \ \_\
    \/_________/ \/_/     \/_/     \/_____/ \/_/    \_\/         \/_/
"#;

fn ensure_valid_id(id: &str) -> Result<&str, ActixError> {
    if id.contains(':') {
        Err(ErrorBadRequest("Invalid ID"))
    } else {
        Ok(id)
    }
}

async fn create_bucket(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, )>,
) -> Result<HttpResponse, ActixError> {
    state.check_admin_credentials(&request)?;
    let bucket = ensure_valid_id(path.0.as_str())?;
    state.create_bucket(bucket).await?;
    Ok(HttpResponse::Created().into())
}

async fn delete_bucket(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, )>,
) -> Result<HttpResponse, ActixError> {
    state.check_admin_credentials(&request)?;
    let bucket = ensure_valid_id(path.0.as_str())?;
    state.delete_bucket(bucket).await?;
    Ok(HttpResponse::NoContent().into())
}

async fn bucket_add_key(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, ActixError> {
    state.check_admin_credentials(&request)?;
    let bucket = ensure_valid_id(path.0.as_str())?;
    let api_key = ensure_valid_id(path.1.as_str())?;
    if state.bucket_add_key(bucket, api_key).await? {
        Ok(HttpResponse::Created().into())
    } else {
        Ok(HttpResponse::NoContent().into())
    }
}

async fn bucket_remove_key(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, ActixError> {
    state.check_admin_credentials(&request)?;
    let bucket = ensure_valid_id(path.0.as_str())?;
    let api_key = ensure_valid_id(path.1.as_str())?;
    if state.bucket_remove_key(bucket, api_key).await? {
        Ok(HttpResponse::NoContent().into())
    } else {
        Ok(HttpResponse::NotFound().into())
    }
}

async fn bucket_list_keys(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, )>,
) -> Result<web::Json<Vec<String>>, ActixError> {
    state.check_admin_credentials(&request)?;
    let bucket = ensure_valid_id(path.0.as_str())?;
    let api_keys = state.bucket_list_keys(bucket).await?;
    Ok(web::Json(api_keys))
}

async fn upload_object(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, )>,
    mut body: web::Payload,
) -> Result<String, ActixError> {
    let bucket = ensure_valid_id(path.0.as_str())?;
    if !state.check_bucket(bucket).await? {
        return Err(ErrorForbidden("Forbidden"));
    }

    state.bucket_check_credentials(&request, bucket).await?;

    let content_type = request.content_type().to_owned();
    let creation_date = Utc::now();

    let (path, file) = state.new_temp_file().await?;
    let mut buffered_file = BufWriter::new(file);
    let mut hasher = Hasher::new();

    while let Some(item) = body.next().await {
        let hasher = &mut hasher;
        let chunk = item?;
        hasher.update(chunk.as_ref());
        buffered_file
            .write(chunk.as_ref())
            .await
            .map_err(ErrorInternalServerError)?;
    }

    buffered_file.flush().await?;

    let file_hash = hasher.finalize().to_hex();
    state
        .make_persistent(path.as_path(), file_hash.as_ref())
        .await
        .map_err(ErrorInternalServerError)?;

    hasher.update(content_type.as_bytes());
    let object_hash = hasher.finalize().to_hex();

    let metadata = ObjectMetadata {
        content_type,
        creation_date,
        file: file_hash.to_string(),
    };

    if state
        .set_object_metadata(bucket, object_hash.as_str(), &metadata)
        .await?
    {
        state.incr_file_ref(file_hash.as_str()).await?;
    }

    let uri = format!(
        "{}\n",
        request
            .url_for("object", &[bucket, object_hash.as_ref()])?
            .path()
    );
    Ok(uri)
}

async fn download_object(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, ActixError> {
    let bucket = ensure_valid_id(path.0.as_str())?;
    let object = ensure_valid_id(path.1.as_str())?;

    let metadata = state
        .get_object_metadata(bucket, object)
        .await?
        .ok_or_else(|| ErrorNotFound("Not Found".to_string()))?;

    let cached = {
        let headers = request.headers();
        headers.contains_key(IF_MODIFIED_SINCE)
    };

    let mut response = if cached {
        HttpResponse::NotModified().into()
    } else {
        let file = state
            .fetch_file(metadata.file.as_str())?
            .set_content_type(
                metadata
                    .content_type
                    .parse()
                    .map_err(ErrorInternalServerError)?,
            )
            .disable_content_disposition()
            .use_etag(false);
        file.into_response(&request)?
    };

    let headers = response.headers_mut();
    headers.insert(
        CACHE_CONTROL,
        "public, max-age=365000000, immutable".parse()?,
    );
    headers.insert(LAST_MODIFIED, metadata.creation_date.to_rfc2822().parse()?);
    if !cached {
        headers.insert(CONTENT_TYPE, metadata.content_type.parse()?);
    }

    Ok(response)
}

async fn delete_object(
    request: web::HttpRequest,
    state: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<String, ActixError> {
    let bucket = ensure_valid_id(path.0.as_str())?;
    let object = ensure_valid_id(path.1.as_str())?;

    state.bucket_check_credentials(&request, bucket).await?;

    let metadata = state
        .get_object_metadata(bucket, object)
        .await?
        .ok_or_else(|| ErrorNotFound("Not Found".to_string()))?;

    let path = state.storage_path_from_id(metadata.file.as_str());

    if state.delete_object_metadata(bucket, object).await? {
        if 0 == state.decr_file_ref(metadata.file.as_str()).await? {
            remove_file(path).await?;
        }
        Ok("Deleted\n".to_string())
    } else {
        Err(ErrorNotFound("Not Found\n".to_string()))
    }
}

#[actix_rt::main]
async fn main() {
    use std::error::Error;

    if let Err(error) = run().await {
        let code = error.as_exit_code();
        eprintln!("Error: {}", error);
        let mut error = error.source();
        while let Some(cause) = error {
            eprintln!("⤷ caused by: {}", &cause);
            error = cause.source();
        }
        std::process::exit(code);
    }
}

async fn run() -> Result<(), Error> {
    env_logger::init();

    dotenv().ok();
    let config = Config::init().map_err(|e| Error::Configuration(ConfigurationError::Env(e)))?;

    if config.api_key.len() < 32 {
        return Err(Error::Configuration(ConfigurationError::Other(format!(
            "Invalid configuration: api key must be at least 32 chars long: {}",
            &config.api_key,
        ))));
    }

    let app_state = AppState::new(&config).await?;

    // Create storage folders
    let tmp = app_state.storage_temp_path();
    create_dir_all(tmp).await?;

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(Compress::default())
            .data(app_state.clone())
            .route("/health-check", web::get().to(HttpResponse::Ok))
            .service(
                web::resource("/b/{bucket}")
                    .route(web::put().to(create_bucket))
                    .route(web::post().to(upload_object))
                    .route(web::delete().to(delete_bucket)),
            )
            .service(web::resource("/b/{bucket}/keys").route(web::get().to(bucket_list_keys)))
            .service(
                web::resource("/b/{bucket}/keys/{key}")
                    .route(web::put().to(bucket_add_key))
                    .route(web::delete().to(bucket_remove_key)),
            )
            .service(
                web::resource("/b/{bucket}/{object}")
                    .name("object")
                    .route(web::get().to(download_object))
                    .route(web::delete().to(delete_object)),
            )
    });

    let server = if let Some(workers) = config.workers {
        server.workers(workers)
    } else {
        server
    };

    let (server, ssl) = match (&config.tls_cert, &config.tls_key) {
        (Some(cert), Some(key)) => {
            let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls())?;
            builder.set_private_key_file(key, SslFiletype::PEM)?;
            builder.set_certificate_chain_file(cert)?;
            (server.bind_openssl(config.http_addr(), builder), true)
        }
        _ => (server.bind(config.http_addr()), false),
    };

    match server {
        Ok(server) => {
            println!("{}", BANNER);
            println!("\tundry {}\n", VERSION);
            println!("\tListening on {}", config.http_addr());
            if ssl {
                println!("\tSSL enabled\n");
            } else {
                println!("\n");
            }
            server.run().await?;
            Ok(())
        }
        Err(e) => Err(Error::Binding(e)),
    }
}
