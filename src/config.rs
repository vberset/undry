use async_std::io::Error as IOError;
use async_std::net::{SocketAddr, ToSocketAddrs};

use envconfig::Envconfig;
use envconfig_derive::Envconfig;

/// Configuration structure
///
/// The configuration can be loaded from the environment with the `init` method
/// of the `Envconfig` trait.
#[derive(Envconfig)]
pub(crate) struct Config {
    #[envconfig(from = "UNDRY_HTTP_INTERFACE", default = "127.0.0.1")]
    pub http_interface: String,
    #[envconfig(from = "UNDRY_HTTP_PORT", default = "8080")]
    pub http_port: u16,
    #[envconfig(from = "UNDRY_REDIS_HOST")]
    pub redis_host: String,
    #[envconfig(from = "UNDRY_REDIS_PORT", default = "6379")]
    pub redis_port: u16,
    #[envconfig(from = "UNDRY_REDIS_AUTH")]
    pub redis_auth: Option<String>,
    #[envconfig(from = "UNDRY_REDIS_SELECT", default = "0")]
    pub redis_select: u16,
    #[envconfig(from = "UNDRY_STORAGE_PATH")]
    pub storage_path: String,
    #[envconfig(from = "UNDRY_API_KEY")]
    pub api_key: String,
    #[envconfig(from = "UNDRY_TLS_CERT")]
    pub tls_cert: Option<String>,
    #[envconfig(from = "UNDRY_TLS_KEY")]
    pub tls_key: Option<String>,
    #[envconfig(from = "UNDRY_WORKERS")]
    pub workers: Option<usize>,
}

impl Config {
    /// Builds the HTTP interface's socket address from `http_interface` and
    /// `http_port`.
    pub fn http_addr(&self) -> SocketAddr {
        SocketAddr::new(
            self.http_interface
                .parse()
                .expect("invalid network interface"),
            self.http_port,
        )
    }

    /// Builds the Redis server's socket address from `redis_host` and
    /// `redis_port`. If `redis_host` is a domain name, it's resolved, then
    /// the first retrieved IP address is used.
    pub async fn redis_addr(&self) -> Result<Option<SocketAddr>, IOError> {
        Ok((self.redis_host.as_str(), self.redis_port)
            .to_socket_addrs()
            .await?
            .next())
    }
}
