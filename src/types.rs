use chrono::{DateTime, Utc};

#[derive(Debug, PartialEq)]
pub(crate) struct ObjectMetadata {
    pub content_type: String,
    pub creation_date: DateTime<Utc>,
    pub file: String,
}
