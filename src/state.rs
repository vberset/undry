use async_std::fs::{create_dir_all, rename, File, OpenOptions};
use async_std::path::{Path, PathBuf};

use actix_files as fs;
use actix_web::error::ErrorForbidden;
use actix_web::http::header::AUTHORIZATION;
use actix_web::{web, Error as ActixError};
use chrono::{DateTime, Utc};
use rand::distributions::{Alphanumeric, Distribution};
use rand::thread_rng;
use redis_async::client::{self, PairedConnection};
use redis_async::resp_array;

use crate::config::Config;
use crate::error::Error;
use crate::types::ObjectMetadata;

static BUCKETS: &str = "buckets";

#[derive(Clone)]
pub(crate) struct AppState {
    api_key: String,
    redis: PairedConnection,
    storage_root: PathBuf,
}

/// `AppState` exposes methods to manage buckets, authorizations, objects,
/// and files. Handle database and file system access.
impl AppState {
    /// Create a fresh application state.
    ///
    /// Initialize redis connection.
    pub async fn new(config: &Config) -> Result<Self, Error> {
        let redis_addr = config
            .redis_addr()
            .await?
            .ok_or_else(|| Error::Network("unable to resolve redis' hostname".to_string()))?;

        let redis = client::paired_connect(&redis_addr).await?;

        if let Some(auth) = &config.redis_auth {
            redis.send::<String>(resp_array!["AUTH", auth]).await?;
        }

        Ok(Self {
            api_key: config.api_key.clone(),
            storage_root: config.storage_path.clone().into(),
            redis,
        })
    }

    /// Returns the path of the storage's temporary files.
    pub fn storage_temp_path(&self) -> PathBuf {
        let mut path = self.storage_root.clone();
        path.push("tmp");
        path
    }

    /// Returns file's path inside the storage.
    pub fn storage_path_from_id(&self, id: &str) -> PathBuf {
        let mut path = self.storage_root.clone();
        path.push(&id[0..2]);
        path.push(&id[2..4]);
        path.push(id);
        path
    }

    /// Create a new tempory file and returns its path and file handler.
    pub async fn new_temp_file(&self) -> Result<(PathBuf, File), Error> {
        let rng = thread_rng();
        let filename: String = Alphanumeric.sample_iter(rng).take(32).collect();

        let mut path = self.storage_temp_path();
        path.push(filename);

        let file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&path)
            .await?;

        Ok((path, file))
    }

    /// Moves a temporary file to persistent storage.
    pub async fn make_persistent(&self, src: &Path, digest: &str) -> Result<(), Error> {
        let dst = self.storage_path_from_id(digest);
        create_dir_all(dst.parent().unwrap()).await?;
        rename(src, dst).await?;
        Ok(())
    }

    /// Returns stream object for the specified file.
    pub fn fetch_file(&self, id: &str) -> Result<fs::NamedFile, Error> {
        let path = self.storage_path_from_id(id);
        let file = fs::NamedFile::open(path)?;
        Ok(file)
    }

    /// Fetch object's metadata.
    pub async fn get_object_metadata(
        &self,
        bucket: &str,
        object: &str,
    ) -> Result<Option<ObjectMetadata>, Error> {
        let key = format!("object:{}:{}", bucket, object);

        let metadata = self
            .redis
            .send::<Option<(String, String, String)>>(resp_array![
                "HMGET",
                key,
                "content_type",
                "creation_date",
                "file"
            ])
            .await
            .map_err(Into::<Error>::into)?;

        metadata
            .map(|(ctype, date, file)| {
                let date = DateTime::parse_from_rfc3339(&date)?;

                Ok(ObjectMetadata {
                    content_type: ctype,
                    creation_date: date.with_timezone(&Utc),
                    file,
                })
            })
            .transpose()
    }

    /// Store object's metadata.
    pub async fn set_object_metadata(
        &self,
        bucket: &str,
        object: &str,
        metadata: &ObjectMetadata,
    ) -> Result<bool, Error> {
        let key = format!("object:{}:{}", bucket, object);

        let date = metadata.creation_date.to_rfc3339();
        let created = self
            .redis
            .send::<bool>(resp_array!["HSETNX", &key, "creation_date", date])
            .await
            .map_err(Into::<Error>::into)?;

        if created {
            self.redis
                .send::<i64>(resp_array![
                    "HSET",
                    &key,
                    "content_type",
                    &metadata.content_type,
                    "file",
                    &metadata.file
                ])
                .await
                .map_err(Into::<Error>::into)?;
        }

        Ok(created)
    }

    /// Delete object's metadata.
    pub async fn delete_object_metadata(&self, bucket: &str, object: &str) -> Result<bool, Error> {
        let key = format!("object:{}:{}", bucket, object);
        self.redis
            .send::<u32>(resp_array!["DEL", key])
            .await
            .map(|c| c == 1)
            .map_err(Into::<Error>::into)
    }

    /// Extract credential from HTTP request and check admin authorization.
    pub fn check_admin_credentials(&self, request: &web::HttpRequest) -> Result<(), ActixError> {
        let credentials = request
            .headers()
            .get(AUTHORIZATION)
            .map(|content_type| content_type.to_str().unwrap());
        match credentials {
            None => Err(ErrorForbidden("Missing API key")),
            Some(cred) if cred == self.api_key => Ok(()),
            _ => Err(ErrorForbidden("Invalid API key")),
        }
    }

    /// Creates an empty bucket with the specified name.
    pub async fn create_bucket(&self, bucket: &str) -> Result<bool, Error> {
        self.redis
            .send::<bool>(resp_array!["SADD", BUCKETS, bucket])
            .await
            .map_err(Into::<Error>::into)
    }

    /// Deletes the given bucket.
    pub async fn delete_bucket(&self, bucket: &str) -> Result<(), Error> {
        let pattern = format!("object:{}:*", bucket);

        let mut scan = 0;
        loop {
            let (index, keys) = self.redis
                .send::<(u32, Vec<String>)>(resp_array!["SCAN", scan, &pattern])
                .await
                .map_err(Into::<Error>::into)?;

            scan = index as usize;
            let command = resp_array!["DEL"].append(keys);
            self.redis.send::<u32>(command)
                .await
                .map_err(Into::<Error>::into)?;

            if scan == 0 {
                break;
            }
        }

        Ok(())
    }

    /// Check the given bucket.
    pub async fn check_bucket(&self, bucket: &str) -> Result<bool, Error> {
        self.redis
            .send::<bool>(resp_array!["SISMEMBER", BUCKETS, bucket])
            .await
            .map_err(Into::<Error>::into)
    }

    /// Add authorization key to the given bucket.
    pub async fn bucket_add_key(&self, bucket: &str, api_key: &str) -> Result<bool, Error> {
        let key = format!("bucket:{}:keys", bucket);
        self.redis
            .send::<i64>(resp_array!["SADD", key, api_key])
            .await
            .map(|c| c == 1)
            .map_err(Into::<Error>::into)
    }

    /// Remove authorization key from the given bucket.
    pub async fn bucket_remove_key(&self, bucket: &str, api_key: &str) -> Result<bool, Error> {
        let key = format!("bucket:{}:keys", bucket);
        self.redis
            .send::<i64>(resp_array!["SREM", key, api_key])
            .await
            .map(|c| c == 1)
            .map_err(Into::<Error>::into)
    }

    /// Returns list of authorization keys for the given bucket.
    pub async fn bucket_list_keys(&self, bucket: &str) -> Result<Vec<String>, Error> {
        let key = format!("bucket:{}:keys", bucket);
        self.redis
            .send::<Vec<String>>(resp_array!["SMEMBERS", key])
            .await
            .map_err(Into::<Error>::into)
    }

    /// Extract credential from the given HTTP request and check user authorization.
    pub async fn bucket_check_credentials(
        &self,
        request: &web::HttpRequest,
        bucket: &str,
    ) -> Result<(), ActixError> {
        let key = format!("bucket:{}:keys", bucket);
        let credentials = request
            .headers()
            .get(AUTHORIZATION)
            .map(|content_type| content_type.to_str().unwrap());
        match credentials {
            None => Err(ErrorForbidden("Missing API key")),
            Some(api_key) => {
                let is_valid = self
                    .redis
                    .send::<bool>(resp_array!["SISMEMBER", key, api_key])
                    .await
                    .map_err(Into::<Error>::into)?;
                if is_valid {
                    Ok(())
                } else {
                    Err(ErrorForbidden("Invalid API key"))
                }
            }
        }
    }

    /// Increment file's references counter.
    pub async fn incr_file_ref(&self, file: &str) -> Result<i64, Error> {
        let key = format!("file-ref:{}", file);
        self.redis
            .send::<i64>(resp_array!["INCR", key])
            .await
            .map_err(Into::<Error>::into)
    }

    /// Decrement file's references counter. Delete related file when
    /// counter reach 0.
    pub async fn decr_file_ref(&self, file: &str) -> Result<i64, Error> {
        let key = format!("file-ref:{}", file);
        self.redis
            .send::<i64>(resp_array!["DECR", key])
            .await
            .map_err(Into::<Error>::into)
    }
}
