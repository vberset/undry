use actix_web::ResponseError;
use async_std::io::Error as IOError;
use chrono::ParseError;
use envconfig::Error as EnvError;
use openssl::error::ErrorStack;
use redis_async::error::Error as RedisError;
use std::fmt;

use Error::*;

enum ErrorExitCode {
    Config = 1,
    Binding = 2,
    Redis = 3,
    IO = 4,
    Network = 5,
}

#[derive(Debug)]
pub enum Error {
    Configuration(ConfigurationError),
    Query(RedisError),
    ParseData(ParseError),
    IO(IOError),
    Binding(IOError),
    Network(String),
    Ssl(ErrorStack),
}

impl Error {
    pub fn as_exit_code(&self) -> i32 {
        (match self {
            Configuration(_) => ErrorExitCode::Config,
            Query(_) => ErrorExitCode::Redis,
            ParseData(_) => ErrorExitCode::Redis,
            IO(_) => ErrorExitCode::IO,
            Binding(_) => ErrorExitCode::Binding,
            Network(_) => ErrorExitCode::Network,
            Ssl(_) => ErrorExitCode::Network,
        }) as i32
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Configuration(ref e) => e.fmt(f),
            Query(ref e) => e.fmt(f),
            ParseData(ref e) => e.fmt(f),
            IO(ref e) => e.fmt(f),
            Binding(ref e) => e.fmt(f),
            Network(ref s) => s.fmt(f),
            Ssl(ref s) => s.fmt(f),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Configuration(_) => None,
            Query(ref e) => Some(e),
            ParseData(ref e) => Some(e),
            IO(ref e) => Some(e),
            Binding(ref e) => Some(e),
            Network(_) => None,
            Ssl(ref e) => Some(e),
        }
    }
}

// Status 500 on database error
impl ResponseError for Error {}

impl From<RedisError> for Error {
    fn from(err: RedisError) -> Error {
        Error::Query(err)
    }
}

impl From<ParseError> for Error {
    fn from(err: ParseError) -> Error {
        Error::ParseData(err)
    }
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IO(err)
    }
}

impl From<EnvError> for Error {
    fn from(err: EnvError) -> Error {
        Error::Configuration(ConfigurationError::Env(err))
    }
}

impl From<ErrorStack> for Error {
    fn from(err: ErrorStack) -> Error {
        Error::Ssl(err)
    }
}

#[derive(Debug)]
pub enum ConfigurationError {
    Env(EnvError),
    Other(String),
}

impl fmt::Display for ConfigurationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ConfigurationError::Env(ref e) => e.fmt(f),
            ConfigurationError::Other(ref s) => s.fmt(f),
        }
    }
}

impl std::error::Error for ConfigurationError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            ConfigurationError::Env(_) => None,
            ConfigurationError::Other(_) => None,
        }
    }
}
