# Undry

*Immutable, opaque storage for web assets*

`undry` is a web service for efficient files storage and distribution.
Stored files are immutable and exposed though opaque URLs, allowing optimal
cache control.

## Features

* Cache-friendly, immutable object storage
* Opaque, immutable URLs
* Automatic file deduplication
* Multi-bucket storage, with per-bucket ACL
* HTTP/1.1 and HTTP/2 support
* Blazing fast, low-latency server

## Installation

### From sources

To build `undry` from source, you need Rust 1.39 or higher. You can then use cargo to build it:

1. Clone the repository:
    ```
    git clone git@gitlab.com:vberset/undry.git
    ```
   
2. Move to the cloned folder:
    ```
    cd undry
    ```
   
3. Build and install `undry`:
    ```
    cargo install --path .
    ```

## Configuration

`undry` is configurable through environment variables and/or `.env` file.
An example of `.env` file - named `.env.dist` is given in the sources.

The available parameters are:

* `UNDRY_HTTP_INTERFACE`: listened interface, default value `127.0.0.1`;
* `UNDRY_HTTP_PORT`: default value `8080`;
* `UNDRY_REDIS_HOST`: redis address, **mandatory**;
* `UNDRY_REDIS_PORT`: redis port, default value `6379`;
* `UNDRY_REDIS_AUTH`: redis authentication password, *optional*;
* `UNDRY_REDIS_SELECT`: redis logical database, default value: `0`;
* `UNDRY_STORAGE_PATH`: files storage folder, **mandatory**;
* `UNDRY_API_KEY`: admin's API key, **mandatory**, can be generated with `openssl rand -hex 32`;
* `UNDRY_TLS_KEY`: TLS private key, *optional*;
* `UNDRY_TLS_CERT`: TLS certificate, *optional*;
* `UNDRY_WORKERS`: number of workers, *optional*, default value: number of logical CPUs in the system.

## Running the server

`undry` needs a [redis](https://redis.io/) server to work. It's up to you
to install and configure it.

Simply run:

```
undry
```

or to display logs:

```
RUST_LOG=info undry
```

## API

### Authentication

The authentication is currently pretty basic. It's based on API keys,
sent through the `Authorization` header on each request.
Hence, `undry` must be used with SSL to be secure.

There is two distinct types of keys:
1. The **admin** key is defined at startup through the `UNDRY_API_KEY` env var.
   It's used to manage buckets and related ACL.
2. **user** keys are associated with bucket to authorize file creation and deletion.

### Bucket and access management

Bucket and access management require the admin API key.

* Create a new bucket
  `PUT /b/<bucket name>`
  
  Example:
  ```
  curl -v -X PUT localhost:8080/b/my-bucket -H "Authorization: my-admin-api-key"
  ```
* Delete a bucket
  `DELETE /b/<bucket name>`
  
  Example:
  ```
  curl -v -X DELETE localhost:8080/b/my-bucket -H "Authorization: my-admin-api-key"
  ```
* List available API key in a bucket
  `GET /b/<bucket name>/keys`
  
  Example:
  ```
  curl -v -X GET localhost:8080/b/my-bucket/keys -H "Authorization: my-admin-api-key"
  ```
* Add an API key to a bucket
  `PUT /b/<bucket name>/keys/<API key>`
  
  Example:
  ```
  curl -v -X PUT localhost:8080/b/my-bucket/keys/my-bucket-api-key -H "Authorization: my-admin-api-key"
  ```
* Delete an API key from a bucket
  `DELETE /b/<bucket name>/keys/<API key>`
  
  Example:
  ```
  curl -v -X DELETE localhost:8080/b/my-bucket/keys/my-bucket-api-key -H "Authorization: my-admin-api-key"
  ```
  
### Object management

* Upload an object
  `POST /b/<bucket name>`
  
  * `200`: Returns the URI of the uploaded file
  
  Example:
  ```
  curl -v -X POST localhost:8080/b/my-bucket --data-binary @my-file \
       -H "Content-Type: `file -b --mime-type my-file" \
       -H "Authorization: my-bucket-api-key"
  ```
* Delete a object
  `DELETE /b/<bucket name>/<object id>`
  
  Example:
  ```
  curl -v -X DELETE localhost:8080/b/my-bucket/opaque-hash-of-the-file -H "Authorization: my-bucket-api-key"
  ```
  
### Misc

* Health check
  `GET /health-check`
  
  Example:
  ```
  curl -v -X GET localhost:8080/health-check
  ```

